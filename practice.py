import requests
import os

def translate_it(path_source, path_result, lang_source, lang_result='ru'):
    """
    YANDEX translation plugin

    docs: https://tech.yandex.ru/translate/doc/dg/reference/translate-docpage/

    https://translate.yandex.net/api/v1.5/tr.json/translate ?
    key=<API-ключ>
     & text=<переводимый текст>
     & lang=<направление перевода>
     & [format=<формат текста>]
     & [options=<опции перевода>]
     & [callback=<имя callback-функции>]

    :param text: <str> text for translation.
    :return: <str> translated text.
    """
    with open(path_source, 'rt', encoding='utf8') as file:
        text = file.read()

    url = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
    key = 'trnsl.1.1.20161025T233221Z.47834a66fd7895d0.a95fd4bfde5c1794fa433453956bd261eae80152'

    params = {
        'key': key,
        'lang': f'{lang_source}-{lang_result}',
        'text': text,
    }
    # response = requests.get(url, params=params).json()
    res = requests.post(url, data=params)
    # print(req.status_code)
    # print(req.encoding)
    # print(req.headers)
    # print(req.content)
    # print(req.text)

    response = res.json()
    res_text = ' '.join(response.get('text', []))

    with open(path_result, 'at', encoding='utf8') as file:
        file.write(f'{res_text}\n')

if __name__ == '__main__':
    files = {
        'DE.txt': 'de',
        'ES.txt': 'es',
        'FR.txt': 'fr',
    }

    lang_result = 'ru'

    # path_result = 'result.txt'
    # open(path_result, 'w').close()  # очистим файл результата выполнения

    for path, lang in files.items():
        filename, extension = os.path.splitext(path)
        path_result = f'{filename}-{lang_result}{extension}'
        open(path_result, 'w').close()

        translate_it(path, path_result, lang)

        with open(path_result, 'rt', encoding='utf8') as file:
            result = file.read()
            print(result)

    # with open(path_result, 'rt') as file:
    #     result = file.read()
    #     print(result)
